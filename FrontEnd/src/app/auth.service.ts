import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

interface User {
  email: string;
  password: string;
}

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private _registerUrl = 'http://localhost:4000/api/register';
  private _loginUrl = 'http://localhost:4000/api/login';
  private _todoinUrl = 'http://localhost:4000/api/todos';
  constructor(private http: HttpClient, private _router: Router) {}

  registerUserAuth(user: User) {
    return this.http.post(this._registerUrl, user);
  }

  loginUserAuth(user: User) {
    return this.http.post(this._loginUrl, user);
  }

  loggedIn() {
    return !!localStorage.getItem('token');
  }

  logoutUser() {
    localStorage.removeItem('token');
    this._router.navigate(['/login']);
  }

  getToken() {
    const token = localStorage.getItem('token');
    return token || null;
  }

  getTodoAuth() {
    return this.http.get(this._todoinUrl);
  }

  addTodoAuth(todoTitle: string) {
    const todo = { title: todoTitle };
    return this.http.post(this._todoinUrl, todo);
  }

  deleteTodoAuth(id: string) {
    const deletTodo = `${this._todoinUrl}/${id}`;
    return this.http.delete(deletTodo);
  }
}
