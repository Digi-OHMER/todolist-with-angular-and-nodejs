import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css',
})
export class LoginComponent {
  loginUserData = {
    email: '',
    password: '',
  };

  constructor(private _auth: AuthService, private _router: Router) {}

  LoginUser() {
    this._auth.loginUserAuth(this.loginUserData).subscribe(
      (res) => {
        console.log(res);
        const token = (res as any).token;
        localStorage.setItem('token', token);
        this._router.navigate(['/todo-list']);
      },
      (err) => console.error(err.error)
    );
  }
}
