import { Component } from '@angular/core';
import { AuthService } from '../auth.service';

interface Todo {
  _id: string;
  title: string;
}

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrl: './todo-list.component.css',
})
export class TodoListComponent {
  newTodoTitle: string = '';
  listTodo: Todo[] = [];
  constructor(private _auth: AuthService) {}

  ngOnInit() {
    this.getTodoList();
  }

  getTodoList() {
    this._auth.getTodoAuth().subscribe(
      (res) => (this.listTodo = res as Todo[]),
      (err) => console.log(err)
    );

    // console.log(this.listTodo);
  }

  addTodo() {
    if (this.newTodoTitle.trim().length === 0) {
      return;
    }
    this._auth.addTodoAuth(this.newTodoTitle).subscribe(
      (res) => {
        console.log(res);
        const todo = res as Todo;
        this.listTodo.push({ title: todo.title, _id: todo._id });
        this.newTodoTitle = '';
      },
      (err) => console.error(err.error)
    );
  }

  deleteTodo(id: string) {
    this.listTodo = this.listTodo.filter((todo) => todo._id !== id);
    this._auth.deleteTodoAuth(id).subscribe(
      (res) => console.log(res),
      (err) => console.log(err)
    );
  }
}
