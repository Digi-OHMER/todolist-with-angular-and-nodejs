import { Component } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrl: './register.component.css',
})
export class RegisterComponent {
  registerUserData = {
    email: '',
    password: '',
  };

  constructor(private _auth: AuthService) {}

  RegisterUser() {
    this._auth.registerUserAuth(this.registerUserData).subscribe(
      (res) => {
        console.log(res);
        const token = (res as any).token;
        localStorage.setItem('token', token);
      },
      (err) => console.error(err.error)
    );
  }
}
