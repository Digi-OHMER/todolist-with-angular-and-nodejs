const express = require("express");
const bodyParser = require("body-parser");

const PORT = 4000;
const api = require("./routers/api");
const app = express();
const mongoose = require("mongoose");
const cors = require("cors");
const db =
  "mongodb+srv://admin:1234@todolistdb.isqlrxc.mongodb.net/?retryWrites=true&w=majority&appName=TodoListDB";

async function connectDB() {
  try {
    await mongoose.connect(db);
    console.log("Connected to MongoDB");
  } catch (err) {
    console.error("Error!: " + err);
  }
}
connectDB();
app.use(cors());

app.use(bodyParser.json());
app.listen(PORT, () => {
  console.log("server running on localhost:" + PORT);
});

app.use("/api/", api);

const div = ` <div>Test</div>`;
app.get("/", (req, res) => {
  res.send(div);
});
