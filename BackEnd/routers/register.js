const express = require("express");
const User = require("../models/user");
const router = express.Router();
const jwt = require("jsonwebtoken");

router.get("/", (req, res) => {
  res.send("get register");
});

router.post("/", async (req, res) => {
  try {
    let userData = req.body;
    let user = new User(userData);
    const userDB = await User.findOne({ email: userData.email });
    if (!userDB) {
      const registeredUser = await user.save();
      let payload = { subject: registeredUser._id };
      let token = jwt.sign(payload, "secretKey");
      res.status(200).send({ token });
    } else {
      res.status(401).send("Already exists with this email");
    }
  } catch (error) {
    console.error("Error!: " + error);
    res.status(500).send("Error registering user");
  }
});

module.exports = router;
