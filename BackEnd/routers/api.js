const express = require("express");
const router = express.Router();
const register = require("./register");
const login = require("./login");
const todo = require("./todo");

router.use("/register/", register);
router.use("/login/", login);
router.use("/todos", todo);

router.get("/", (req, res) => {
  res.send("open server");
});

module.exports = router;
