const express = require("express");
const User = require("../models/user");
const router = express.Router();
const jwt = require("jsonwebtoken");

router.get("/", (req, res) => {
  res.send("get Login");
});

router.post("/", async (req, res) => {
  try {
    let userData = req.body;
    const user = await User.findOne({ email: userData.email });
    if (!user) {
      res.status(401).send("invalid email");
    } else if (user.password !== userData.password) {
      res.status(401).send("invalid password");
    } else {
      let payload = { subject: user._id };
      let token = jwt.sign(payload, "secretKey");
      res.status(200).send({ token });
    }
  } catch (err) {
    console.error("Error!: " + err);
    res.status(500).send("Error login user");
  }
});

module.exports = router;
