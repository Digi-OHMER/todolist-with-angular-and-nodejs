const express = require("express");
const router = express.Router();
const TodoType = require("../models/todo");
const verifyToken = require("./authMiddleware");

// Get all todos for a user
router.get("/", verifyToken, async (req, res) => {
  try {
    const todos = await TodoType.find({ userId: req.userId }).select("title");
    res.status(200).send(todos);
  } catch (error) {
    console.error("Error!: " + error);
    res.status(500).send("Error getting todos");
  }
});

// Add a new todo
router.post("/", verifyToken, async (req, res) => {
  try {
    let todoTitle = req.body;
    let todo = new TodoType({
      userId: req.userId,
      title: todoTitle.title,
    });

    const savedTodo = await todo.save();
    res.status(200).send(savedTodo);
  } catch (error) {
    console.error("Error!: " + error);
    res.status(500).send("Error creating todo");
  }
});

// Delete a todo by ID
router.delete("/:id", verifyToken, async (req, res) => {
  try {
    await TodoType.deleteOne({ _id: req.params.id });
    res.status(200).send("Todo deleted successfully");
  } catch (error) {
    console.error("Error deleting todo: " + error);
    res.status(500).send("Error deleting todo");
  }
});

module.exports = router;
